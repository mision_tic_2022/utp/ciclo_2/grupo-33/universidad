package controller;

import model.ConexionDB;
import view.MenuConsola;
import view.MenuJframe;
import view.MenuJoption;

public class MainController {

    public MainController() {
        // CREAR CONEXIÓN A BD
        ConexionDB conexionDB = new ConexionDB();
        // CONSTRUIR CONTROLADORES
        UniversidadController uController = new UniversidadController(conexionDB);
        // CONSTRUIR VISTAS
        // MenuConsola mConsola = new MenuConsola(uController);
        // mConsola.construirMenu();
        // MenuJoption mJoption = new MenuJoption(uController);
        // mJoption.crearMenu();
        MenuJframe mJframe = new MenuJframe(uController);
    }

}
