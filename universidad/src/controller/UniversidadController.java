package controller;

import java.sql.ResultSet;

import model.ConexionDB;
import model.dao.UniversidadDao;

public class UniversidadController {
    // ATRIBUTOS
    private ConexionDB conexionDB;

    // CONSTRUCTOR
    public UniversidadController(ConexionDB conexionDB) {
        this.conexionDB = conexionDB;
    }

    // ACCIONES
    public boolean crearUniversidad(String nombre, String nit, String direccion, String email) {
        // Crear universidad
        UniversidadDao objUni = new UniversidadDao(nombre, nit, direccion, email);
        // Almacenar universidad en la BD
        // return objUni.insert(conexionDB.getConexion());
        boolean insert = objUni.insert(conexionDB.getConexion());
        return insert;
    }

    public ResultSet obtenerUniversidades() {
        return UniversidadDao.getUniversidades(conexionDB.getConexion());
    }

}
