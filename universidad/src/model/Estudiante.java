package model;

public class Estudiante extends Persona {
    // ATRIBUTO
    private String codigo;

    public Estudiante(String nombre, String apellido, int edad, String cedula, char sexo, String codigo) {
        // Envía los parámetros al constructor de la super clase Persona
        super(nombre, apellido, edad, cedula, sexo);
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

}
