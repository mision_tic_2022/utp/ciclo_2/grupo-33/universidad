package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.Universidad;

public class UniversidadDao extends Universidad {

    public UniversidadDao(String nombre, String nit, String direccion, String email) {
        super(nombre, nit, direccion, email);
    }

    /****************************
     * 
     * QUERIES
     * 
     **************************/

    /**
     * Método para el registro de una Universidad en la BD.
     * Retorna un valor booleano (true si la acción es satisfactoria, false en caso
     * contrario)
     */
    public boolean insert(Connection conn) {
        boolean insert = false;
        try {
            // Preparar consulta SQL
            String query = "INSERT INTO universidades VALUES(?,?,?,?)";
            PreparedStatement pst = conn.prepareStatement(query);
            // Setear la consulta SQL
            pst.setString(1, getNit());
            pst.setString(2, getNombre());
            pst.setString(3, getDireccion());
            pst.setString(4, getEmail());
            // Ejecutar
            insert = pst.executeUpdate() == 1;

        } catch (SQLException e) {
            e.printStackTrace();
            // System.out.println(e.getMessage());
        }
        return insert;
    }

    public static ResultSet getUniversidades(Connection conn) {
        ResultSet result = null;
        try {
            String query = "SELECT * FROM universidades";
            Statement st = conn.createStatement();
            result = st.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ResultSet getUniversidadByNit(Connection conn, String nit) {
        ResultSet result = null;
        try {
            String query = "SELECT * FROM universidades WHERE nit = ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, nit);
            result = pst.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean updateUniversidad(Connection conn, String nit, String nombre, String direccion,
            String email) {
        boolean update = false;
        try {
            String query = "UPDATE universidades SET nombre = ?, direccion = ?, email = ? WHERE nit = ?";
            PreparedStatement pst = conn.prepareStatement(query);
            // Setear la consulta
            pst.setString(1, nombre);
            pst.setString(2, direccion);
            pst.setString(3, email);
            pst.setString(4, nit);
            // Ejecutar consulta
            update = pst.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return update;
    }

    /**
     * NOTA:
     * No concatenar variables directamente con una consulta SQL porque genera
     * problemas SQLI (sql Injection)
     * 
     * String user = "";
     * String password = "\" OR 1=1--";
     * 
     * SELECT * FROM users WHERE name = user AND pass = password;
     * 
     * SELECT * FROM users WHERE name = "andres" AND pass = " " OR 1=1-- ";
     * 
     */

}
