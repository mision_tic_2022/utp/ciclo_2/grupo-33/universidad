package model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class Universidad {
    // ATRIBUTOS
    private String nombre;
    private String nit;
    private String direccion;
    private ArrayList<String> telefonos;
    private String email;
    private ArrayList<Facultad> facultades;
    private ArrayList<Estudiante> estudiantes;
    private Map<String, ArrayList<String>> matriculas;

    // CONSTRUCTOR
    public Universidad(String nombre, String nit, String direccion, String email) {
        this.nombre = nombre;
        this.nit = nit;
        this.direccion = direccion;
        this.email = email;
        this.telefonos = new ArrayList<String>();
        this.facultades = new ArrayList<Facultad>();
        this.estudiantes = new ArrayList<Estudiante>();
        this.matriculas = new LinkedHashMap<String, ArrayList<String>>();
    }

    // CONSULTORES
    public String getNombre() {
        return nombre;
    }

    public String getNit() {
        return nit;
    }

    public String getDireccion() {
        return direccion;
    }

    public ArrayList<String> getTelefonos() {
        return telefonos;
    }

    public String getEmail() {
        return email;
    }

    public ArrayList<Facultad> getFacultades() {
        return facultades;
    }

    public ArrayList<Estudiante> getEstudiantes() {
        return estudiantes;
    }

    public Map<String, ArrayList<String>> getMatriculas() {
        return matriculas;
    }

    // MODIFICADORES
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setTelefonos(ArrayList<String> telefonos) {
        this.telefonos = telefonos;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFacultades(ArrayList<Facultad> facultades) {
        this.facultades = facultades;
    }

    public void setEstudiantes(ArrayList<Estudiante> estudiantes) {
        this.estudiantes = estudiantes;
    }

    /*****************************
     * 
     * ACCIONES
     * 
     ***************************/

    public void registrarEstudiante(String nombre, String apellido, int edad, String cedula, char sexo, String codigo) {
        // Crear objeto de tipo estudiante
        Estudiante estudiante = new Estudiante(nombre, apellido, edad, cedula, sexo, codigo);
        // Almacenar objeto en el array
        this.estudiantes.add(estudiante);
    }

    public void crearFacultad(String codigo, String nombre) {
        // Crear objeto de tipo Facultad
        // Facultad facultad = new Facultad(codigo, nombre);
        // this.facultades.add(facultad);
        this.facultades.add(new Facultad(codigo, nombre));
    }

    public void matricularEstudiante(String cedula, String codigoFacultad) {
        // Buscar si hay por lo menos un estudiante matriculado en la facultad
        if (matriculas.containsKey(codigoFacultad)) {
            // Obtener arrayList y adicionar nuevo elemento (cedula del estudiante)
            matriculas.get(codigoFacultad).add(cedula);
        } else {
            // Crear objeto que contendrá el Map como valor
            ArrayList<String> cedulas = new ArrayList<String>();
            // Adicionar cedula al arrayList
            cedulas.add(cedula);
            // Relacionar el codigoFacultad con la cedula del estudiante
            matriculas.put(codigoFacultad, cedulas);
        }
    }

}
