package view;

import java.sql.ResultSet;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import controller.UniversidadController;

public class MostrarUniversidadesJframe extends JFrame {

        // ATRIBUTOS
        private UniversidadController uController;
        private JTable tabla;
        private DefaultTableModel modelo;

        // CONSTRUCTOR
        public MostrarUniversidadesJframe(UniversidadController uController) {
                this.uController = uController;
                // Configurar propiedades de la Ventana
                setTitle("Universidades registradas");
                // setBounds(600, 200, 420, 500);
                init();
                // Ajusta el tamaño de la ventana
                pack();
                setVisible(true);
        }

        public void init() {
                Object columns[] = new Object[] { "Nit", "Universidad", "Dirección", "Email" };
                TableModel m = new DefaultTableModel(
                                new Object[][] {},
                                columns);
                // Crear tabla e indicar el modelo a la tabla
                tabla = new JTable(m);
                // Capturar el modelo de la tabla y castearlo a defaultTableModel
                modelo = (DefaultTableModel) tabla.getModel();
                JScrollPane scroll = new JScrollPane(tabla);
                // Mostrar datos
                mostrarDatos();
                add(scroll);
        }

        public void mostrarDatos() {
                ResultSet results = uController.obtenerUniversidades();
                try {
                        while (results.next()) {
                                String nit = results.getString("nit");
                                String nombre = results.getString("nombre");
                                String direccion = results.getString("direccion");
                                String email = results.getString("email");
                                modelo.addRow(new Object[] { nit, nombre, direccion, email });
                        }
                } catch (Exception e) {
                        e.printStackTrace();
                }
        }

}
