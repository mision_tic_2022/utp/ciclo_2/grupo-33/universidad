package view;

import javax.swing.JOptionPane;

import controller.UniversidadController;

public class MenuJoption {

    // ATRIBUTO
    private UniversidadController uController;

    // CONSTRUCTOR
    public MenuJoption(UniversidadController uController) {
        this.uController = uController;
    }

    public void crearMenu() {
        // Crear objeto UniversidadVista
        UniversidadJoption uJoption = new UniversidadJoption(uController);
        // Opciones del menú
        String mensaje = "1) Crear universidad\n";
        mensaje += "2) Mostrar todas las universidades\n";
        mensaje += "3) Actualizar universidad\n";
        mensaje += "4) Eliminar universidad\n";
        mensaje += "-1) Salir\n";

        try {
            int opcion = 0;
            while (opcion != -1) {
                // Solicitar opción al usuario
                // Castear para convertir a entero
                opcion = Integer.parseInt(JOptionPane.showInputDialog(null, mensaje));
                // Evaluar opcion
                switch (opcion) {
                    case 1:
                        uJoption.crearUniversidad();
                        break;
                    case 2:
                        uJoption.mostrarUniversidades();
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            System.out.println("Por favor ingrese valores numericos en las opciones del menu");
        }
    }

    public void introduccion() {
        // --------Ventanas emergentes--------
        // Mostrar mensaje
        JOptionPane.showMessageDialog(null, "Hola mundo desde un JOption");
        // Solicitar mensaje
        String nombre = JOptionPane.showInputDialog(null, "Por favor ingrese su nombre: ");
        // Mostrar nombre
        JOptionPane.showMessageDialog(null, "Su nombre es " + nombre);
    }

}
