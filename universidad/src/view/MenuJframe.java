package view;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import controller.UniversidadController;

import java.awt.event.*;

public class MenuJframe extends JFrame {

    private UniversidadController uController;

    // ATRIBUTOS
    private JButton btnCrear;
    private JButton btnMostrar;

    // CONSTRUCTOR
    public MenuJframe(UniversidadController uController) {
        this.uController = uController;
        // Añadir titulo
        setTitle("MENU");
        setBounds(600, 200, 320, 300);
        getContentPane().setLayout(null);
        // Indicar que debe cerrar la ejecución del programa cuando se le dé click en la
        // equis(X)
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        // Inicializar atributos
        btnCrear = new JButton("Crear universidad");
        // Inidicar coordenadas del botón y tamaño
        btnCrear.setBounds(20, 20, 120, 40);

        btnMostrar = new JButton("Mostrar universidades");
        btnMostrar.setBounds(150, 20, 150, 40);

        // Manejadores de eventos
        btnCrear.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                eventBtnCrear();
            }
        });
        btnMostrar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new MostrarUniversidadesJframe(uController);
            }
        });

        // Añadir elementos al JFrame
        add(btnCrear);
        add(btnMostrar);

        // Mostrar Jframe
        setVisible(true);
    }

    public void eventBtnCrear() {
        new UniversidadJFrame(uController);
    }

}
