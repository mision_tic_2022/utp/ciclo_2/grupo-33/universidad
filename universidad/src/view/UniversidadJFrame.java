package view;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import java.awt.event.*;

import controller.UniversidadController;

public class UniversidadJFrame extends JFrame {
    // ATRIBUTOS
    private UniversidadController uController;

    // ATRIBUTOS DE LA UI
    private JLabel lblNombre;
    private JLabel lblNit;
    private JLabel lblDireccion;
    private JLabel lblEmail;
    private JTextField txtNombre;
    private JTextField txtNit;
    private JTextField txtDireccion;
    private JTextField txtEmail;
    private JButton btnCrear;

    // CONSTRUCTOR
    public UniversidadJFrame(UniversidadController uController) {
        this.uController = uController;
        // Configuración de la ventana
        setTitle("Formulario");
        setBounds(600, 200, 250, 300);
        getContentPane().setLayout(null);
        // Inicializar elementos
        init();

        // Mostrar JFrame
        setVisible(true);
    }

    public void init() {
        lblNombre = new JLabel("Nombre: ");
        lblNombre.setBounds(20, 20, 80, 30);

        txtNombre = new JTextField();
        txtNombre.setBounds(100, 20, 100, 30);

        lblNit = new JLabel("Nit: ");
        lblNit.setBounds(20, 60, 80, 30);

        txtNit = new JTextField();
        txtNit.setBounds(100, 60, 100, 30);

        lblDireccion = new JLabel("Dirección: ");
        lblDireccion.setBounds(20, 100, 80, 30);

        txtDireccion = new JTextField();
        txtDireccion.setBounds(100, 100, 100, 30);

        lblEmail = new JLabel("Email: ");
        lblEmail.setBounds(20, 140, 100, 30);

        txtEmail = new JTextField();
        txtEmail.setBounds(100, 140, 100, 30);

        btnCrear = new JButton("Crear");
        btnCrear.setBounds(100, 200, 100, 40);

        // Añadir manejador de eventos
        btnCrear.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                eventBtnCrear();
            }
        });

        // Añadir elementos a la UI
        add(lblNombre);
        add(txtNombre);
        add(lblNit);
        add(txtNit);
        add(lblDireccion);
        add(txtDireccion);
        add(lblEmail);
        add(txtEmail);
        add(btnCrear);
    }

    public void eventBtnCrear() {
        boolean crear = uController.crearUniversidad(txtNombre.getText(), txtNit.getText(), txtDireccion.getText(),
                txtEmail.getText());
        if (crear) {
            limpiar();
            JOptionPane.showMessageDialog(this, "Universidad creada con éxito");
        } else {
            JOptionPane.showMessageDialog(this, "Ups! algo sucedió, intenta mas tarde");
        }
    }

    public void limpiar() {
        txtNombre.setText("");
        txtNit.setText("");
        txtDireccion.setText("");
        txtEmail.setText("");
    }

}
