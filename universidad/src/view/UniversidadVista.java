package view;

import java.sql.ResultSet;
import java.util.Scanner;

import controller.UniversidadController;

public class UniversidadVista {
    // ATRIBUTOS
    private UniversidadController uController;

    public UniversidadVista(UniversidadController uController) {
        this.uController = uController;
    }

    public void crearUniversidad(Scanner sc) {
        System.out.println(crearEncabezado("crear universidad"));
        // Solicitar nombre
        System.out.println("Nombre: ");
        String nombre = sc.next();
        sc.nextLine();
        // Solicitar nit
        System.out.println("Nit: ");
        String nit = sc.next();
        sc.nextLine();
        // Solicitar dirección
        System.out.println("Direccion: ");
        String direccion = sc.next();
        sc.nextLine();
        // Solicitar email
        System.out.println("Email: ");
        String email = sc.next();
        sc.nextLine();
        // Crear universidad
        boolean insert = uController.crearUniversidad(nombre, nit, direccion, email);
        if (insert) {
            System.out.println("Universidad creada con exito");
        } else {
            System.out.println("Por favor intenta mas tarde");
        }
    }

    public String crearEncabezado(String mensaje) {
        // Concatenar mensaje y ponerlo en mayúscula
        String encabezado = "-----------" + mensaje.toUpperCase() + "---------\n";
        encabezado += "Por favor ingrese la siguiente información: \n";
        return encabezado;
    }

    public void mostrarUniversidades() {
        try {
            ResultSet result = uController.obtenerUniversidades();
            String info = "-------------UNIVERSIDADES---------------\n";
            while (result.next()) {
                String nit = result.getString("nit");
                String nombre = result.getString("nombre");
                info += "Nit: " + nit;
                info += "\nNombre: " + nombre;
                info += "\n---------------------------------\n";
            }
            System.out.println(info);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 
     * public static void obtenerUniversidadXnit(ConexionDB objConn) {
     * ResultSet result = Universidad.getUniversidadByNit(objConn.getConexion(),
     * "12345");
     * try {
     * if (result.next()) {
     * String nit = result.getString("nit");
     * String nombre = result.getString("nombre");
     * String direccion = result.getString("direccion");
     * String email = result.getString("email");
     * // Concatenar información
     * String info = "Nit: " + nit;
     * info += "\nNombre: " + nombre;
     * info += "\nDireccion: " + direccion;
     * info += "\nEmail: " + email;
     * System.out.println(info);
     * }
     * } catch (SQLException e) {
     * e.printStackTrace();
     * }
     * }
     * 
     */

}
