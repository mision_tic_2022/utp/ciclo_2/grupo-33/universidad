package view;

import java.sql.ResultSet;

import javax.swing.JOptionPane;

import controller.UniversidadController;

public class UniversidadJoption {
    // ATRIBUTOS
    private UniversidadController uController;

    public UniversidadJoption(UniversidadController uController) {
        this.uController = uController;
    }

    public void crearUniversidad() {
        String encabezado = crearEncabezado("crear universidad");
        // SOLICITAR DATOS
        String nombre = JOptionPane.showInputDialog(null, encabezado + "Nombre:");
        String nit = JOptionPane.showInputDialog(null, encabezado + "Nit:");
        String direccion = JOptionPane.showInputDialog(null, encabezado + "Dirección:");
        String email = JOptionPane.showInputDialog(null, encabezado + "Email: ");

        // Crear universidad
        boolean insert = uController.crearUniversidad(nombre, nit, direccion, email);
        if (insert) {
            JOptionPane.showMessageDialog(null, "Universidad creada con exito");
        } else {
            JOptionPane.showMessageDialog(null, "Por favor intenta mas tarde");
        }
    }

    public String crearEncabezado(String mensaje) {
        // Concatenar mensaje y ponerlo en mayúscula
        String encabezado = "-----------" + mensaje.toUpperCase() + "---------\n";
        encabezado += "Por favor ingrese la siguiente información: \n";
        return encabezado;
    }

    public void mostrarUniversidades() {
        try {
            ResultSet result = uController.obtenerUniversidades();
            String info = "-------------UNIVERSIDADES---------------\n";
            while (result.next()) {
                String nit = result.getString("nit");
                String nombre = result.getString("nombre");
                info += "Nit: " + nit;
                info += "\nNombre: " + nombre;
                info += "\n---------------------------------\n";
            }
            JOptionPane.showMessageDialog(null, info);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
